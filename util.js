"use strict";
const fs = require("fs");

module.exports = {
    readFile: (name) => new Promise((resolve, reject) => fs.readFile(name, "utf8", (err, data) => 
        err ? reject(err) : resolve(data))),
    last: (arr) => arr[arr.length-1]
};
