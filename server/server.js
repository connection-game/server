"use strict";
const express = require("express");
const expressWs = require("express-ws");
const Session = require("./session.js").Session;
const LevelServer = require("./levelServer/levelServer.js").LevelServer;
const last = require("../util.js").last;

class Server {
    constructor(config) {
        this.config = config;
        this.sessions = [];
        this.lastSession = undefined;

        this.app = express();
        this.webSockets = expressWs(this.app);
        this.app.listen(config.server.port, () => console.log(`listening on ${config.server.port}`));

        this.levelServer = new LevelServer(config.server.mongo, config.server.dbName, config.server.collectionName);
        this.initEndpoints();

        this.app.use((err, req, res, next) => {
            console.error(err.stack);
            res.status(500).send('unknown_error');
        });
    }

    initEndpoints() {
        this.app.get("/status", (req, res) => this.sendStatus(req, res));
        this.app.get("/", (req, res) => res.redirect("/client/"));
        this.app.ws("/", (ws, req) => this.newConnection(ws));
        this.app.use(express.static(this.config.server.staticDirectory));
        this.levelServer.connectionPromise.then(() => {
            this.app.use("/levels", this.levelServer.router);
        });
    }

    sendStatus(req, res) {
        res.send(JSON.stringify({
            status: "ok",
            sessions: this.sessions.length
        }));
    }

    newConnection(socket) {
        console.log(this.sessions.length);

        if (this.lastSession != undefined && this.lastSession.room.connections.length == 0) {
            this.sessions.splice(this.sessions.findIndex(s => s == this.lastSession), 1);
            this.lastSession = undefined;
        }

        if (this.lastSession == undefined) {
            let session = new Session();
            session.addConnection(socket);
            session.setOnAllDisconnected(() => {
                this.sessions.splice(this.sessions.findIndex(s => s == session), 1);
            });
            this.sessions.push(session);
            this.lastSession = session;
        } else {
            this.lastSession.addConnection(socket);
            this.lastSession = undefined;
        }
    }
}

module.exports.Server = Server;
