"use strict";
const express = require("express");
const LevelsDb = require("./levelsDb.js").LevelsDb;

class LevelServer {
    constructor(url = "mongodb://localhost:27017", dbName = "connection", collectionName = "levels") {
        this.levelsDb = new LevelsDb(url, dbName, collectionName);
        this.router = express.Router();

        this.connectionPromise = this.levelsDb.connectionPromise;
        this.connectionPromise.then((client) => {
            this.initEndpoints();
        });
    }

    initEndpoints() {
        this.router.use(express.json());
        this.router.get("/", this.getLevelNames.bind(this));
        this.router.get("/:name", this.getLevelByName.bind(this));
        this.router.post("/:name", this.addLevel.bind(this));
        this.router.delete("/:name", this.deleteLevelByName.bind(this));
    }

    async getLevelNames(req, res) {
        try {
            res.send(JSON.stringify(await this.levelsDb.getLevelNames()));
        } catch (e) {
            console.log(e);
            res.send(JSON.stringify({ "error": "unknown_error" }));
        }
    }

    async addLevel(req, res) {
        try {
            console.log(req.body);
            console.log(await this.levelsDb.addLevel(req.params.name, req.body));
            res.send(JSON.stringify({ "message": "success" }));
        } catch (e) {
            console.log(e);
            res.send(JSON.stringify({ "error": "unknown_error" }));
        }
    }

    async getLevelByName(req, res) {
        try {
            res.send(JSON.stringify(await this.levelsDb.getLevelByName(req.params.name)));
        } catch (e) {
            console.log(e);
            res.send(JSON.stringify({ "error": "unknown_error" }));
        }
    }

    async deleteLevelByName(req, res) {
        try {
            res.send(JSON.stringify(await this.levelsDb.deleteLevelByName(req.params.name)));
        } catch (e) {
            console.log(e);
            res.send(JSON.stringify({ "error": "unknown_error" }));
        }
    }
}

module.exports.LevelServer = LevelServer;
