"use strict";
const mongo = require("mongodb").MongoClient;

class LevelsDb {
    constructor(url = "mongodb://localhost:27017", dbName = "connection", collectionName = "levels") {
        this.connectionPromise = mongo.connect(url);
        this.connectionPromise.then((client) => {
            this.db = client.db(dbName);
            this.levels = this.db.collection(collectionName);
        });
    }

    async getLevelNames() {
        return await this.levels.find().map(n => n.name).toArray();
    }

    async addLevel(levelName, data) {
        return await this.levels.replaceOne({ name: { $eq: levelName } }, { name: levelName, data: data }, { upsert: true });
    }

    async getLevelByName(name) {
        return await this.levels.findOne({ name: name });
    }

    async deleteLevelByName(name) {
        return await this.levels.deleteOne({ name: name });
    }
}

module.exports.LevelsDb = LevelsDb;
