"use strict";
const WebsocketRoom = require("./websocketRoom.js").WebsocketRoom;
const Protocol = require("./libconnection/protocol.js");

class Session {
    constructor() {
        this.room = new WebsocketRoom();
    }

    addConnection(connection) {
        this.room.addConnection(connection);
        this.room.broadcastJSON(new Protocol.PlayerConnected(connection.uuid), connection);
        connection.send(JSON.stringify(new Protocol.PlayerID(connection.uuid)));
        connection.send(JSON.stringify(new Protocol.RoomInfo(connection.uuid, this.room.connections.length, this.room.uuid)));

        connection.on("message", (data) => this.handleMessage(data, connection));
        connection.on("close", (code, reason) => this.connectionClosed(code, reason, connection));
    }

    connectionClosed(code, reason, connection) {
        this.room.broadcastJSON(new Protocol.PlayerDisconnected(connection.uuid));
        console.log("connection closed", this.room.connections.length);
    }

    setOnAllDisconnected(fun) {
        this.room.setOnAllDisconnected(fun);
    }

    handleMessage(data, connection) {
        try {
            let message = JSON.parse(data);
            switch(message.to) {
            case Protocol.To.broadcast:
                this.switchMessage(message, connection);
                break;
            case Protocol.To.server:
                this.handleServerMessage(message, connection);
                break;
            default:
                connection.sendJSON(new Protocol.Error("unknownTo"));
                break;
            }
        } catch(e) {
            console.log(e);
            connection.sendJSON(new Protocol.Error(connection.uuid, Protocol.Errors.parseError));
        }
    }

    switchMessage(message, connection) {
        message.from = connection.uuid; //add real from
        this.room.broadcastJSON(message, connection);
    }

    handleServerMessage(message, connection) {
        //currently there are no server handled messages
    }
}

module.exports.Session = Session;
