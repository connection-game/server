"use strcit";
const uuid = require("uuid/v4");

class WebsocketRoom {
    constructor() {
        this.connections = [];
        this.uuid = uuid();
    }

    addConnection(connection) {
        connection.uuid = uuid();
        connection.sendJSON = (data) => connection.send(JSON.stringify(data));

        this.connections.push(connection);
        connection.on("close", () => {
            this.connections.splice(this.connections.findIndex(c => c == connection), 1);
            if(this.connections.length == 0) {
                this.onAllDisconnected();
            }
        });
    }

    setOnAllDisconnected(fun) {
        this.onAllDisconnected = fun;
    }

    broadcast(message, except) {
        this.connections.forEach(c => {
            if(c !== except) {
                c.send(message);
            }
        });
    }

    broadcastJSON(message, except) {
        this.broadcast(JSON.stringify(message), except);
    }
}

module.exports.WebsocketRoom = WebsocketRoom;
