"use strict";
const server = require("./server/server.js");
const fs = require("fs");
const util = require("./util.js");

util.readFile("config.json")
    .then(data => {
        new server.Server(JSON.parse(data));
    })
    .catch(err => {
        console.log(err);
    });
