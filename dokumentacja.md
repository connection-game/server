# Dokumentacja

1. Rozpakowujemy paczkę z projektem
2. Otwieramy plik config.json
3. Ustawiamy wartość `port` na numer portu pod którym ma działać serwer
4. Ustawiamy wartość `mongodb` na string połączenia do bazy danych mongo
5. Otwieramy okno wiersza poleceń
6. Uruchamiamy polecenie `npm install` żeby zainstalować pakiety
7. Uruchamiamy serwer poleceniem `node main.js`
