# Dokumentacja

1. Pobranie wszystkich modułów, `git submodule init`, `git submodule update`, `git pull`
2. Stworzenie pliku config.json tak jak config-example.json.
    `port` - port na którym działa serwer.
    `mongodb` - string połączenia do bazy danych mongo
    `dbName` - nazwa bazy danych MongoDB
    `collectionName` - nazwa kolekcji w bazie danych
3. Uruchamienie polecenia `npm install` aby zainstalować pakiety
4. Uruchamienie serwera poleceniem `node main.js`

`adres_serwera:$port/levelEditor` - edytor poziomów
`localhost:8080/levelEditor`

Aby gra działała poprawnie musi być zainstalowana baza MongoDB.

Tutoriale są dostępne w pliku `tutorial.json`. Należy zaimportować je do bazy danych. Polecenie: `mongoimport --db $dbName --collection $collectionName --file path\to\tutorial.json`

### Sterowanie
* `WSAD` lub strzałki - sterowanie graczem
* `C` - wyczyszczenie laseru z `connectora`
* `Spacja` - podnoszenie danego przedmiotu (np. `boxa`)
* `Esc` - lista z poziomami

### LevelEditor:
* Laser `laser` - Może przybierać różne kolory, jest wrażliwy na obiekty przecinające jego trasę i nie może przez nie przejść (wyjątek: szkło). 
* Łącznik `connector` - element przekazujący dalej laser w określonych przez gracza kierunkach (do konkretnych obiektów), służy jako wzmacniacz mocy lasera
* Skrzynka `box` - Gracz nie może jej podnieść, ale może ją przepchnąć. Często blokuje trasę lasera lub służy do trzymania przycisku
* Przycisk `button` - Służy najczęściej do otwierania zamkniętych drzwi. Do jego działania potrzebny jest ciągły nacisk przez gracza lub jakikolwiek obiekt postawiony na nim (są połączone poprzez ID)
* Odbiornik `receiver` - Miejsce do którego należy dostarczyć laser
    * Odbiornik ogólny - kończy grę (nie mają konkretnego ID)
    * Odbiorniki otwierające drzwi (są połączone poprzez ID)
* Drzwi `door` - Można je otwierać, przeszkadzają w przejsciu gracza i laseru (są połączone poprzez ID)
* Generator `generator` - generuje laser w wybranym kierunku
* Helper `helper` - Przydatne informacje dla graczy
* Ściany `walls`
    * `wall` - zwykła ściana, nic przez nią nie przechodzi
    * `glass` - laser przechodzi, gracz nie
    * `fog` - laser nie przechodzi, gracz tak